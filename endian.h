/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef MYSQL_BINLOG_ENDIAN_H
#define MYSQL_BINLOG_ENDIAN_H

/**
 * @brief NOTICE
 * Some macro for generate the value from little to big endian or for another
 */
#include <fc_string.h>

#define GLOBAL_VARS
GLOBAL_VARS int IS_BIGENDIAN;

#define int32_cbit(i) ( ((i) & 0x000000ffu) << 24 | ( (i) & 0x0000ff00u ) << 8 | ( ((i) & 0x00ff0000u) >> 8) | ( (i) & 0xff000000u ) >> 24 )
#define int64_cbit(i) ( int32_cbit((i) & 0x0ffffffffu) << 32 | int32_cbit( ((i) >> 32) & 0x0ffffffffu) )

static
int big_endian() {
    union {
        short  a:2;
        char b[2];
    } ua;
    ua.b[0] = 0x11;
    ua.b[1] = 0x22;
    
    if ( (ua.a & 0x0ff) == 0x11 ) {
        return 1; /* Big endian */
    } else {
        return 0; /* Little endian */
    }
}

long long exchange_copy_data_from_little(char *little_memory, int num); /* Only used for MySQL DATE type */
long long copy_data_from_little(char *little_memory, int num);

#endif /* MYSQL_BINLOG_ENDIAN_H */
