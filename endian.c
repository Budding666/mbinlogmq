/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 *
 * This is the very import file to get data from the MySQL little endian stream
 * Just doing it, to ensure the System's Little-Big endian rule.
 */

#include <endian.h>

/**
 * @brief NOTICE
 * This function will copy memory data from the given little_memory order with the given num
 */
long long exchange_copy_data_from_little(char *little_memory, int num)
{
    int           j, k;
    union {
    char      a[8];
    long long b:64;
    } Data;
    
    e_memzero(&Data, sizeof(Data));
    if ( big_endian() )
    {
        /**
         * @brief NOTICE
         * Big endian
         */
        for ( j = 0; j < num; j++ )
        {
            Data.a[j] = little_memory[j];
        }
    }
    else
    {
        /**
         * @brief NOTICE
         * Little endian
         */
        for ( j = 0, k = num; j < k; j++ )
        {
            Data.a[--num] = little_memory[j];
        }
    }
    return Data.b;
}

/**
 * @brief NOTICE
 * This function will copy memory data from the given little_memory order with the given num
 */
long long copy_data_from_little(char *little_memory, int num)
{
    int           j, k;
    union {
        unsigned char a[8];
        long long     b:64;
    } Data;
    
    e_memzero(&Data, sizeof(Data));
    if ( big_endian() )
    {
/**
 * @brief NOTICE
 * Big endian
 */
        for ( j = 0, k = num; j < k; j++ )
        {
            Data.a[--num] = little_memory[j];
        }
    }
    else
    {
/**
 * @brief NOTICE
 * Little endian
 */

        for ( j = 0; j < num; j++ )
        {
            Data.a[j] = little_memory[j];
        }
    }
    return Data.b;
}